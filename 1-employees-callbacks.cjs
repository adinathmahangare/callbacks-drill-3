/*
 * Use asynchronous callbacks ONLY wherever possible.
 * Error first callbacks must be used always.
 * Each question's output has to be stored in a json file.
 * Each output file has to be separate.

 * Ensure that error handling is well tested.
 * After one question is solved, only then must the next one be executed. 
 * If there is an error at any point, the subsequent solutions must not get executed.
   
 * Store the given data into data.json
 * Read the data from data.json
 * Perfom the following operations.

    1. Retrieve data for ids : [2, 13, 23].
    2. Group data based on companies.
        { "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
    3. Get all data for company Powerpuff Brigade
    4. Remove entry with id 2.
    5. Sort data based on company name. If the company name is same, use id as the secondary sort metric.
    6. Swap position of companies with id 93 and id 92.
    7. For every employee whose id is even, add the birthday to their information. The birthday can be the current date found using `Date`.

    NOTE: Do not change the name of this file

*/

const fs = require('fs');
const dataIds = [2, 13, 23];

function employeesCallback(dataIds) {

    fs.readFile('./data.json', 'utf8', (err, data) => {
        if (err) {
            console.log('Data not found');
            return ;
        } else {

            const employeesData = JSON.parse(data);

            const foundEmployeesByID = employeesData['employees'].filter((employee) => {

                if (dataIds.includes(employee.id)) {
                    return employee;
                }
            });

            fs.writeFile('foundEmpployeesByID.json', JSON.stringify(foundEmployeesByID) , (err) => {
                if (err) {
                    console.log('No employees found based in given ids');
                    return;
                } else {
                    const groupedEmployees = employeesData['employees'].reduce((acc, employee) => {

                        company =  employee.company;
                        
                        if (!acc[company]) {
                            acc[company] = [employee.name];
                        } else {
                            acc[company].push(employee.name);
                        }
    
                        return acc;
                    }, {});

                    fs.writeFile('groupedEmployees.json', JSON.stringify(groupedEmployees), (err) => {
                        if (err) {
                            console.log('No employees found for grouping');
                            return ;
                        } else {

                            const powerpuffBrigadeInfo = employeesData['employees'].filter((employee) => {

                                if (employee.company === 'Powerpuff Brigade') {
                                    return employee;
                                }
                            });

                            fs.writeFile('powerpuffBridageInfo.json', JSON.stringify(powerpuffBrigadeInfo), (err) => {
                                if (err) {
                                    console.log('Error while writing powerpuffBridgeInfo');
                                    return ;
                                } else {

                                    const dataWithoutId2 = employeesData['employees'].filter((employee) => {
                                        if(employee.id !== 2) {
                                            return employee;
                                        }
                                    });

                                    fs.writeFile('dataWithoutId2.json', JSON.stringify(dataWithoutId2), (err) => {
                                        if (err) {
                                            console.log('Error while writing dataWithoutId2');
                                            return;
                                        } else {

                                            const sortedData = dataWithoutId2.sort((employee1, employee2) => {

                                                if (employee1.company > employee2.company){
                                                    return 1;
                                                } else if (employee1.company < employee2.company){
                                                    return -1;
                                                } else {
                                                    return employee1.id - employee2.id;
                                                }
                                            });

                                            fs.writeFile('sortedData.json', JSON.stringify(sortedData), (err) => {
                                                if (err) {
                                                    console.log('Error while returning sortedData');
                                                    return ;
                                                } else {
                                                    
                                                    const employeeData92 = sortedData.find((employee) => {
                                                        if (employee.id === 92) {
                                                            return employee;
                                                        }
                                                    })
                    
                                                    const employeeData93 = sortedData.find((employee) => {
                                                        if (employee.id === 93) {
                                                            return employee;
                                                        }
                                                    })
                    
                                                    const swappedEmployeeData = sortedData.map((employee) => {
                                                        if (employee.id == 92) {
                                                            return employeeData93;
                                                        }
                    
                                                        if (employee.id == 93) {
                                                            return  employeeData92;
                                                        }
                    
                                                        return employee;
                                                    });

                                                    fs.writeFile('swappedEmployeesData.json', JSON.stringify(swappedEmployeeData), (err) => {
                                                        if (err) {
                                                            console.log('Error while swapping data');
                                                            return ;
                                                        } else {
                                                            const currentDateString = new Date().toLocaleDateString();
                                    
                                                            const employeeWithEvenIdsBirthDates = swappedEmployeeData.map((employee) => {
                                                                if (employee.id % 2 === 0) {
                                                                    employee.birthdate = currentDateString;
                                                                }
                                                                return employee;
                                                            })

                                                            fs.writeFile('employeeWithEvenIdsBirthDates.json', JSON.stringify(employeeWithEvenIdsBirthDates), (err) => {
                                                                if (err) {
                                                                    console.log('Error while getting employeeWithEvenIdsBirthdates');
                                                                    return ;
                                                                } else {
                                                                    return ;
                                                                }
                                                            });
                                                        }
                                                    });
                                                }
                                            });
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

employeesCallback(dataIds);


